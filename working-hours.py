#!/usr/bin/env python

from datetime import date, timedelta

def calculate_working_days(d1, d2):
    delta = d2 - d1
    week_day1 = d1.weekday()
    week_day2 = d2.weekday()
    overflow = (7 - week_day1) + (week_day2)
    return (((delta.days - overflow) / 7) * 5) + (5 - week_day1 + week_day2 + 1)

def distribute_working_hours(start_date, end_date, total_hours):
    working_days = calculate_working_days(start_date, end_date)
    return total_hours / working_days

print(distribute_working_hours(date(2024, 2, 12), date(2024, 5, 10), 464))

