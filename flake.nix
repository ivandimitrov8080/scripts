{
  description = ''
  '';

  inputs = {
    nixpkgs.url = "nixpkgs";
    ide = {
      url = "github:ivandimitrov8080/flake-ide";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, ide, ... }:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
      nvim = ide.nvim.${system}.standalone {
        plugins = {
          lsp.servers = {
            pylsp.enable = true;
            bashls.enable = true;
          };
        };
      };
      buildInputs = with pkgs; [
        nvim
      ];
    in
    {
      devShell.${system} = pkgs.mkShell {
        inherit buildInputs;
      };
    };
}

